function [z, vz, J, dJdt, n] = SemiclassicalDriftCurrent( E, t, w, ng, units)
% HELP SemiclassicalDriftCurrent - Calculates the current and dipole moment 
% for an ionized gas.
%    
% USAGE
%
%   [z, vz, J, dJdt, n] = SemiclassicalDriftCurrent( E, t);
%   [z, vz, J, dJdt, n] = SemiclassicalDriftCurrent( E, t, w);
%   [z, vz, J, dJdt, n] = SemiclassicalDriftCurrent( E, t, w, ng);
%   [z, vz, J, dJdt, n] = SemiclassicalDriftCurrent( E, t, w, ng, units);
%  
% INPUT
% 
%   E - The electric field. This should be an array where each element
%   corresponds to the electric field at the moment of time of the
%   corresponding elements of the time array, t. The electric field can be
%   entered using either SI or atomic units. Specify the units by setting 
%   units to 'SI' or 'au'.
%
%   t - This is the time coordinate associated with the electric field. 
%       The units can be entered as either SI or AU. Specify the units by 
%       setting units to 'SI' or 'au'.
%
%   w - This is a function that calculates the ionization rate in units of 
%       Hz. This must be an instantanous ionization rate. The code does
%       not yet have the ability to cycle-average the field. 
%
%   ng - Background gas density in m^-3. I believe that z and vz are 
%       independent of ng. 
%
%   units - The units of E, t, z, and vz. You can choose 'SI' or 'au' while
%   the default is 'au'.
%
% OUTPUT
%
%   z - Ensemble averaged free electron displacement. Will have units
%   specified by 'units'.
%   vz - Ensemble averaged free electron velocity Will have units specified 
%        by 'units'.
%   J, dJdt, n - Will have SI units.  
% 
% EXAMPLE
%
% From Kim's 2009 Paper (http://dx.doi.org/10.1063/1.3134422)
%
%   ng = 2.4e25;    % [m^-3]
%   theta = pi/2;
%   t = linspace(-0.5e-12, 0.5e-12, 2^14);
%   E =  exp(-2*log(2)*(t/50e-15).^2).*( 2.8e10*cos(2*pi*375e12*t) + 1.2e10*cos(2*pi*750e12*t + theta) );
%
%   [~, vz, J,~ ,n] = SemiclassicalDriftCurrent( E, t, @(F) w_landau(F,'N2'), ng, 'SI');
%
%   figure; plot(t/1e-15, E/1e11); xlim([-50,100]); xlabel('t (fs)'); ylabel('E (GV/cm)')
%   figure; plot(t/1e-15, n/1e6); xlim([-50,100]); xlabel('t (fs)'); ylabel('n (cm^{-3})')
%   figure; plot(t/1e-15, J/1e10); xlim([-50,100]); xlabel('t (fs)'); ylabel('J (MA/cm^2)')
%   figure; plot(t/1e-15, vz); xlim([-50,100]); xlabel('t (fs)'); ylabel('v (m/s)')
%
%
% Other examples
%   
%   [z, vz] = SemiclassicalDriftCurrent( E, t, @(F) w_landau_default(F,'H'));
%   
%
% AUTHOR:  Luke A. Johnson (lukealanjohnson@gmail.com) 
% DATE: Dec. 12 2013

if nargin < 5;
    units = 'au';
end
if nargin < 4;
    ng = 2.5e25;                    %[m^-3] atmospheric density
end
if nargin < 3;
    w = @(field) w_landau_default(field,'H');      %[Hz] adiabatic ionization for H
end

%% Physical Constants
me = 9.1e-31; %[kg]
e =  1.6e-19; %[C] 
   
%% Convert input from atomic units to SI  (THOMAS LOOK AT THE ROUNDING IN THIS CONVERSION)
% See http://en.wikipedia.org/wiki/Atomic_units
if strcmpi('au',units);
    E = E*5.14220652e11;     
    t = t*2.418884326505e-17;    
end

%%% all variable are in SI units at this point %%%

%% Time Domain Parameters
Nt = length(E);
dt = t(2)-t(1);                        % [s]


% initialize arrays
n = zeros(size(E));
dJdt = zeros(size(E));
J = zeros(size(E));   
z = zeros(size(E));
vz = zeros(size(E));
    
%% Solve for the free electron density   
% Time Centered Method
%
% Solve: dn/dt = w(E) * ( ng - n(t) )
%
n(1) = 0;   % Assume density is initially zero.
wj = 0;  % Ionization Rate for j=1. Assume field is initially zero 
for j=1:Nt-1
    % Calc ionization rate at j+1 step    
    wjp1 = w(E(j+1));
    n(j+1) = (n(j) * (1 - 0.5*dt*wj) + 0.5*dt*( wjp1 + wj ) * ng )/(1 + 0.5*dt*wjp1);
    wj = wjp1;
end


%% Rate of change of current    
dJdt = (e^2/me) * n .* E ;

%% Current
% Time Centered Method
%
% Solve: dJ/dt =(e^2/me) * n(t) * E(t)
%
J(1)=0;     % Assume current starts at zero.
for j=1:Nt-1
    J(j+1) = J(j) + 0.5*dt*(dJdt(j+1) + dJdt(j));
end

%% Polarization
% Maxwell's Equ. in SI
%   D = epsilon0 E + P      
%   curl(H) = J + dD/dt     
%   curl(H) - epsilon0 dE/dt = J + dP/dt
%            /t
%   P_J(t) = | dt' J(t')       with initial condition that P_J(t=-infty)=0
%            /
% where P_J is the polarization from a free current. 

% Time Centered Method
%
% Solve: dP/dt = J(t) 
%
P(1)=0;     % Assume polarization starts at zero.
for j=1:Nt-1
    P(j+1) = P(j) + 0.5*dt*(J(j+1) + J(j));
end

%% Ensemble averaged free electron displacement and velocity
% 
% P = - e n <z>
% J = - e n <vz> 
%
for j=1:Nt;
    if n(j) == 0;
        z(j) = 0;
        vz(j) = 0;
    else
        z(j)  = P(j) / (-e*n(j));
        vz(j) = J(j) / (-e*n(j)); 
    end
end

%% Convert output from SI to atomic units
% See http://en.wikipedia.org/wiki/Atomic_units
if strcmpi('au',units);
    a0 = 5.291772192e-11;
    z = z / a0;     
    vz = vz / 2.1876912633e6;    
end

end





function w = w_landau_default(E,atom)
%% HELP w_adk - ADK ionization rate  
% E - Electric field with units of V/cm
% atom - a string that describes the atoms or molecules that are of
% interest
%
% For ionization energies see 
% http://en.wikipedia.org/wiki/Ionization_energies_of_the_elements_%28data_page%29
%%
wa = 4.134e16;      %[Hz]
Ea = 5.14e11;       %[V/m]
UH = 13.59792;      %[eV]
if strcmpi(atom,'H')
    r = 1;
elseif strcmpi(atom,'He')
    Ui = 24.58741;    %[eV]
    r = Ui/UH;  
elseif strcmpi(atom,'Ar')
    Ui = 15.75962;    %[eV] 
    r = Ui/UH;    
elseif strcmpi(atom,'N2')
    Ui = 15.576;    %[eV] From kim 2009
    r = Ui/UH;
elseif strcmpi(atom,'N2plus')
    Ui = 59.5;    %[eV] From Dai, J. Opt. 13 (2011) 055201
    r = Ui/UH;
elseif strcmpi(atom,'O2')
    Ui = 12.07;    %[eV] From Dundas and Rost (2005)
    r = Ui/UH;
end

if abs(E/Ea) <= eps
    w = 0;
else
    w = 4 * wa *r^(2.5) * abs(Ea ./ E) .* exp( - (2/3) * r^(1.5) * abs(Ea./E) );
end

end
