function w = w_landau(E,atom)
%% HELP w_landau - Landau (adiabatic) ionization rate 
% E - Electric field with units of V/cm
% atom - a string that describes the atoms or molecules that are of
% interest
%
%%
wa = 4.134e16;      %[Hz]
Ea = 5.14e11;       %[V/m]
UH = 13.59792;      %[eV]
if strcmpi(atom,'H')
    r = 1;
elseif strcmpi(atom,'He')
    Ui = 24.58741;    %[eV]
    r = Ui/UH;  
elseif strcmpi(atom,'Ar')
    Ui = 15.75962;    %[eV] 
    r = Ui/UH;
elseif strcmpi(atom,'N2')
    Ui = 15.576;    %[eV] From kim 2009
    r = Ui/UH;
elseif strcmpi(atom,'N2plus')
    Ui = 59.5;    %[eV] From Dai, J. Opt. 13 (2011) 055201
    r = Ui/UH;
elseif strcmpi(atom,'O2')
    Ui = 12.07;    %[eV] From Dundas and Rost (2005)
    r = Ui/UH;
end

if abs(E/Ea) <= eps
    w = 0;
else
    w = 4 * wa *r^(2.5) * abs(Ea ./ E) .* exp( - (2/3) * r^(1.5) * abs(Ea./E) );
end

end