function w = w_pmpb(E, lambda, ionp, Z, nmax, tol)
%% PMPB Ionization model
% See Popruzhenko, Mur, Popov, Bauer. PRL 101, 193003 (2008)
% 
%% USAGE
%
% w = w_pmpb( E, lambda, ionp, Z)
%
%% INPUT
% 
% E - An array of N real values for the electric field. The electric field 
%     should have unit of Volts/meter. 
% 
% lambda -  The wavelength of the electric field's carrier frequency.
%           Should have units of meters.
% 
% ionp - Ionizization potential energy in units of eV
% 
% Z - Some effective parameter with no units. Should typically be around 1. 
%
% Typical Parameters from Talebpour (1999)
%     N2 - Ionization Potential: 15.576 eV
%     N2 - Effective Coulomb barrier (Z_eff/r): Z_eff = 0.9
%     O2 - Ionization Potential: 12.063 eV
%     O2 - Effective Coulomb barrier (Z_eff/r): Z_eff = 0.53
% 
%% OUTPUT
%
% w - Ionization rate in an array of length N. Should have units of Hz.
% 
%% EXAMPLE
%
%    Need Example
%
%% FUTURE WORK
%
%  * Improve constants used in converting between A.U. and S.I. 
%  * Add while loop to "infinite" sum
%  * Add proper erf
%
%% AUTHOR:  
%
%   John Palastro
%   Luke A. Johnson (lukealanjohnson@gmail.com)
%   Thomas Rensink
%
%% DATE: 2013-12-13
%%
%MODIFIED 3/20 (johnsonl): Added ability for w_pmpb2 to accept a variable         
%number of arguments. Added help documentation.                                   

%MODIFIED 3/20 (trensink): sum continues until the difference between             
%successive is less than some fraction (tol) of the nth partial sum, or until a   
%maximum nth partial sum (nmax).  Also, the Faddevva erf package replaces         
%the erfz for speed, see: http://ab-initio.mit.edu/wiki/index.php/Faddeeva_Package 

% default input parameters
if nargin < 4;
	Z = 1;
end
if nargin < 5;
	nmax = 100;      % maximum number of terms in sum
end
if nargin < 6;
	tol = 1e-2;     % tolerance
end

if exist('Faddeeva_erfi','file') == 3
    erfi = @(z) Faddeeva_erfi(z);        
    % prefer to use this version if it is found.
else
    erfi = @(z) erfi_default(z);
    % This version is found at the bottom of this file.
    warning(['Faddeeva_erfi mex file not found.',...
             ' Using default erfi function.']);
end

% physical constants
SPEEDOFLIGHT  =  299792458.0;                     % [m/s]
MU0           =  1.256637061435917e-06;           % [N/A^2]
EPSILON0      =  8.854187817620391e-12;           % [F/m]
UNITCHARGE    =  1.602176565e-19;                 % [C]
ELECTRONMASS  =  9.10938291e-31;                  % [kg]
PROTONMASS    =  1.67262158e-27;                  % [kg]
HBAR          =  1.05457e-34;                     % [J-s]

hbar=HBAR/UNITCHARGE;  %hbar in eV*s
cl= SPEEDOFLIGHT;    %speed of light
eps0=EPSILON0;% permitivity in Farads / m 

% use to match john exactly
%hbar=6.582e-16;  %hbar in eV*s
%cl=3e8;    %speed of light
%eps0=8.8542e-12;% permitivity in Farads / m		       
%ionH = 13.6;  %John and I used 13.6 but Thomas needs 13.46 rounded to 13.5

omg=2*pi*cl/lambda;

%%%%%%%%%%%%%%%%%%%%%%%%
% PMPB Ionization Rate %
%%%%%%%%%%%%%%%%%%%%%%%%

%converting E2 to E in atomic units
EAU=abs(E)/5.14e11;

%converting ionization potential to atomic units
ionpAU=ionp*(UNITCHARGE/4.36e-18);
%ionpAU=ionp*(1.6e-19/4.36e-18);  %use to match john exactly
% ionpAU[au] = ionp[eV] (1.6e-19[J/eV])/(ionH[J]) 
% ionH = me^4/hbar^2[gaussian] = 4.4e-11[erg] = 4.4e-18[J]=27.2[eV]

%converting frequency to atomic units
omgAU=omg*2.42e-17;

%ionization model parameters
K0=ionp/(hbar*omg);
nstar=Z/sqrt(2*ionpAU);
C2=2^(2*nstar-2)/(gamma(nstar+1))^2;

nI = length(E);
wAU=zeros(1,nI);
garr=zeros(1,nI);
qcount = 0;
for i=1:nI;

    F=EAU(i)/(2*ionpAU)^(1.5);

    gam=sqrt(2*ionpAU)*omgAU/EAU(i);

    Q=(2/F)^(2*nstar)*(1+2*exp(-1)*gam)^(-2*nstar);  % PMPB Coulumb Correction
    %Q=(2/F)^2;                                      % PPT Coulumb Correction
    
    beta=2*gam/sqrt(1+gam^2);

    gee=1.5/gam*((1+0.5/gam^2)*asinh(gam)-0.5*sqrt(1+gam^2)/gam);

    c1=asinh(gam)-gam/sqrt(1+gam^2);

    nth=K0*(1+0.5/gam^2);

 	sum	= 0;
	prevsum = 0;
	n		= floor(nth)+1;
	while abs(sum-prevsum) >= tol*sum && n <= floor(nth) + nmax;
		%for tol = .00001, nmax 300, well converged for 40fs sin2 env, 2000nm,
		%2e14 W/cm2, 13,6eV, (Z = 1).  More terms needed as lambda increases.
		
		arg=sqrt(beta*(n-nth));
        
		DI=0.5*sqrt(pi)*exp(-arg^2)*erfi(arg);
		
		prevsum = sum;
		sum = sum+DI*exp(-(2/3)*gee/F-2*c1*(n-nth));
		
		n = n + 1;
	end
	
	q = (floor(nth) + nmax) - n + 1;
	if q <= 0 && qcount == 0
        warning('The tolerance was not met. Consider increasing nmax.');
		qcount = qcount + 1; % This could be qcount=1? Probably a bit more clear.
	end
	
	wsr=2/pi*C2*ionpAU*K0^(-1.5)*beta^(0.5)*sum;
    
% before Thomas' edits.  Please delete this if nothing comes up.
%     sum=0;
%     for n=floor(nth)+1:floor(nth)+100; 
%         %for nmax =100 the PPT (pre-PMPB) coulomb correction matches the 
%         % adiabatic limit well at I=1e15W/cm^2 when Z=1 and ionp = 15eV.
%         % A better match, requires a larger sum.
%    
%         arg=sqrt(beta*(n-nth));
%         DI=0.5*sqrt(pi)*exp(-arg^2)*erfi(arg);
%         sum=sum+DI*exp(-(2/3)*gee/F-2*c1*(n-nth));
%     end
% 
%     wsr=2/pi*C2*ionpAU*K0^(-1.5)*beta^(0.5)*sum;
% 
    wAU(i)=Q*wsr;
    garr(i)=gam;
    
end
w=wAU/2.42e-17;

fprintf('PMPB Ionization Model:\n');
fprintf('\tlambda = %g[nm]\n',lambda/1e-9);
fprintf('\tionp = %g[eV]\n',ionp);
fprintf('\tZ = %g\n',Z);
fprintf('\tK0 = %g\n',K0);
fprintf('\tnstar = %g\n',nstar);
fprintf('\tC^2 = %g\n',C2);
fprintf('\n');
end

function ans=erfi_default(x)
% %erfi(x). The Imaginary error function, as it is defined in Mathematica
% %erfi(z)==erf(iz)/i (z could be complex) using 
% %the incomplete gamma function in matlab: gammainc
% %Using "@": erfi = @(x) real(-sqrt(-1).*sign(x).*gammainc(-x.^2,1/2))
% %Note: limit(x->0) erfi(x)/x -> 2/sqrt(pi)
%
% %Example 1: 
% x=linspace(0.001,6,100);
% y=exp(-x.^2).*erfi(x)./2./x;
% figure(1), clf;plot(x,y*sqrt(pi))
%
% %Example 2: 
% [x,y]=meshgrid(linspace(-3,3,180),linspace(-3,3,180));
% z=x+i*y;
% figure(1), clf;contourf(x,y,log(erfi(z)))
% axis equal;axis off
xc=5.7;%cut for asymptotic approximation (when x is real)
ans=~isreal(x).*(-(sqrt(-x.^2)./(x+isreal(x))).*gammainc(-x.^2,1/2))+...
    isreal(x).*real(-sqrt(-1).*sign(x).*((x<xc).*gammainc(-x.^2,1/2))+...
    (x>=xc).*exp(x.^2)./x/sqrt(pi));
end