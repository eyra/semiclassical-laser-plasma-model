# README

## SemiclassicalDriftCurrent 
Calculates the current and dipole moment 
 for an ionized gas.
    
### USAGE

   [z, vz, J, dJdt, n] = SemiclassicalDriftCurrent( E, t);
   [z, vz, J, dJdt, n] = SemiclassicalDriftCurrent( E, t, w);
   [z, vz, J, dJdt, n] = SemiclassicalDriftCurrent( E, t, w, ng);
   [z, vz, J, dJdt, n] = SemiclassicalDriftCurrent( E, t, w, ng, units);
  
### INPUT
 
   E - The electric field. This should be an array where each element
   corresponds to the electric field at the moment of time of the
   corresponding elements of the time array, t. The electric field can be
   entered using either SI or atomic units. Specify the units by setting 
   units to 'SI' or 'au'.

   t - This is the time coordinate associated with the electric field. 
       The units can be entered as either SI or AU. Specify the units by 
       setting units to 'SI' or 'au'.

   w - This is a function that calculates the ionization rate in units of 
       Hz. This must be an instantanous ionization rate. The code does
       not yet have the ability to cycle-average the field. 

   ng - Background gas density in m^-3. I believe that z and vz are 
       independent of ng. 

   units - The units of E, t, z, and vz. You can choose 'SI' or 'au' while
   the default is 'au'.

### OUTPUT

   z - Ensemble averaged free electron displacement. Will have units
   specified by 'units'.
   vz - Ensemble averaged free electron velocity Will have units specified 
        by 'units'.
   J, dJdt, n - Will have SI units.  
 
### EXAMPLE

### From Kim's 2009 Paper

   ng = 2.4e25;

   theta = pi/2;
   
   t = linspace(-0.5e-12, 0.5e-12, 2^14);
   
   E =  exp(-2*log(2)*(t/50e-15).^2).*( 2.8e10*cos(2*pi*375e12*t) + 1.2e10*cos(2*pi*750e12*t + theta) );

   [~, vz, J,~ ,n] = SemiclassicalDriftCurrent( E, t, @(F) w_adk_default(F,'N2'), ng, 'SI');

   figure; plot(t/1e-15, E/1e11); xlim([-50,100]); xlabel('t (fs)'); ylabel('E (GV/cm)')
   
   figure; plot(t/1e-15, n/1e6); xlim([-50,100]); xlabel('t (fs)'); ylabel('n (cm^{-3})')
   
   figure; plot(t/1e-15, J/1e10); xlim([-50,100]); xlabel('t (fs)'); ylabel('J (MA/cm^2)')
   
   figure; plot(t/1e-15, vz); xlim([-50,100]); xlabel('t (fs)'); ylabel('v (m/s)')


### Other examples
   
   [z, vz] = SemiclassicalDriftCurrent( E, t, @(F) w_adk_default(F,'H'));
   

### AUTHOR:  Luke A. Johnson (lukealanjohnson@gmail.com) 
### DATE: Dec. 12 2013
