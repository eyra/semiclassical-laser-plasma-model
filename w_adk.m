function w = w_adk(E, atom, instORave)
%% HELP w_adk - ADK ionization rate 
%
% E - Electric field with units of V/cm
% atom - a string that describes the atoms or molecules that are of
% interest OR a vector with the ionization potential as the first elements 
% and Z as an optional second element.
%
% The exact form of the ionization rate is taken from equ. (1) of [1].
% 
% From reference [2]:
%       "states of complex atoms are characterized by effective principle 
%       and orbital quantum numbers nstar and lstar that take into account
%       the quantum effects, and for the states of the atomic ions also the
%       degree of ionization Z.
%
%% EXAMPLES
%
%   w_adk( 1e10)
%   w_adk( 1e10, 'H')
%   w_adk( 1e10, 13.59792)
%   w_adk( 1e10, [13.59792 1])
%
%% References
%
%   [1] Larochelle, Talebpour, Chin 1998 J. Phys. B: At. Mol. Opt. Phys. 31 
%   1201. http://dx.doi.org/10.1088/0953-4075/31/6/008
%
%   [2] Ammosov, Delone, Krainov Sov. Phys. JEPT 64 (6) Dec. 1986
%
%   [3] Talebpour, Yang, Chin Opt. Comm. Vol 163, 1–3, 1999, 29–32
%   http://dx.doi.org/10.1016/S0030-4018(99)00113-3
%
%% Author: Luke Johnson (lukealanjohnson@gmail.com)
%          Thomas Rensink
%% Date: Dec. 18 2013

%% Default Parameters & Error Checking
if nargin < 2
    atom = 'H';
end
if nargin < 3
    instORave = 'inst';
end

% Check to make sure instORave was set properly
if ~strcmpi(instORave, 'inst') && ~strcmpi(instORave, 'ave')
    error(['instORave was set as %s but it must be either inst or ave',...
           ' (case insensitive).'],instORave);
end;

%% Physical Constants
UnitCharge = 1.602176565e-19;   %[C]
HartreeEnergy = 4.35974417e-18; %[J]
AtomicField = 5.14220652e11;    %[V/m]
hbar = 1.05457173e-34;          %[J-s]

%% Gas Ionization Parameters   

% If "atom" is a charactor array       
if ischar(atom)                       
    if strcmpi(atom,'H')
        ionp = 13.59792; %[eV]
        Z = 1;
    elseif strcmpi(atom,'He')
        ionp = 24.58741;    %[eV]
        Z = 1;
    elseif strcmpi(atom,'Ar')
        ionp = 15.75962;    %[eV]
        Z = 1;
    elseif strcmpi(atom,'N2')
        ionp = 15.576;    %[eV] From kim 2009
        Z = 1;
        warning('Not sure if reference [3] supports Z=0.9 for the ADK rate. I believe that Z=1 is better. ')
    elseif strcmpi(atom,'N2plus')
        ionp = 59.5;    %[eV] From Dai, J. Opt. 13 (2011) 055201
        Z = 2;
        warning('Hey are you sure Z=2 is correct? Check this!')
    elseif strcmpi(atom,'O2')
        ionp = 12.07;    %[eV] From Dundas and Rost (2005)
        Z = 0.53;
        warning('Figure 2 of ref. [3] suggests that Z=0.53 is best for ADK O2 model.')
    end
    
    
% If "atom" is a numeric array
elseif isnumeric(atom)          
    % first entry must be ionization potenital
    ionp = atom(1);
    % optional second entry is effective Z
    if length(atom)==1; 
        Z = 1;                  % default
    elseif length(atom)==2;
        Z = atom(2);            %
    end
end
    
%% Convert from SI to Atomic Units
%(http://en.wikipedia.org/wiki/Atomic_units)
F = abs(E) / AtomicField;            % F is the electric field in atomic units
  
%converting ionization potential to atomic units
ionpAU= ionp * (UnitCharge/HartreeEnergy);
% ionpAU[au] = ionp[eV] (1.6e-19[J/eV])/(ionH[J]) 
% ionH = me^4/hbar^2[gaussian] = 4.4e-11[erg] = 4.4e-18[J]=27.2[eV]

%% Effective principle and orbital quantum numbers
nstar = Z / sqrt(2*ionpAU);
m = 0 ;
lstar = round(nstar - 1);

%% Calculate Prefactors: C2nl and flm
% equ. (3) of [1]
C2nl = ( 2^(2*nstar) ) ./ (nstar * gamma(nstar + lstar + 1) * gamma(nstar - lstar) );

% equ. (2) of [1]
flm = (2*lstar + 1) * gamma(lstar + abs(m) + 1) /...
    ( 2^abs(m) * gamma( abs(m) + 1) * gamma( lstar - abs(m) + 1) );

%% Calculate Ionization Rate (Atomic Units)
% equ. (1) of [1]
w_AU = C2nl * flm * ionpAU * sqrt(6/pi)  ...
    .* (2 * (2*ionpAU).^(1.5) ./ F).^(2*nstar - abs(m) - 1.5) ...
    .* exp(- (2 * (2*ionpAU).^(1.5) ) ./ (3 * F));

%% Convert from Atomic Units to SI
w_ave = w_AU  * (HartreeEnergy/hbar);

%% Do you want the cycle-AVEraged or the INSTantanous ionization rate?
if strcmpi(instORave, 'ave')
    w = w_ave;
elseif strcmpi(instORave, 'inst')
    w_inst = (3 * F ./ (pi*(2*ionpAU).^(1.5)) ).^(-1/2) .* w_ave;
    %Uncycle Averaged Ionization Rate. now w(t) depends on E(t)
    w = w_inst;
end

% Zero ionization where field is too small
 w(F<=eps) = 0;
 
end